# chef-docs 翻訳環境構築のための手順

## 1. githubより、chef-docsをクローンします。
[https://github.com/opscode/chef-docs](https://github.com/opscode/chef-docs)

<pre><code>git clone https://github.com/opscode/chef-docs.git</code></pre>

## 2. chef-docsのchef_master用の翻訳ファイルのrepoをclone
[https://bitbucket.org/j_hotta/chef_master_locale](https://bitbucket.org/j_hotta/chef_master_locale)

まず、chef-docsをクローンした後に、上記repoをchef_masterディレクトリーの
中で下記のようにクローしてください。

<pre><code>$ cd chef_master
$ git clone git@bitbucket.org:j_hotta/chef_master_locale.git locale
</code></pre>

## 3. chef-docs内のpotファイルから、poとmoを生成するためのrubyのスクリプト
[https://bitbucket.org/j_hotta/po_manupulate](https://bitbucket.org/j_hotta/po_manupulate)

このスクリプトを、source, trasnlateのあるディレクトリと同じ場所に設置し
てください。

<pre><code>$ cd chef_master
$ git clone git@bitbucket.org:j_hotta/po_manupulate.git scripts
</code></pre>

## 4. conf.pyを編集し、日本語のmoファイルを読み込む設定

<pre><code>$ vi chef_master/soruce/conf.py</code></pre>

追記内容：
<pre><code>　locale_dirs= ["../locale/"]
　language="ja"
</code></pre>

## 5. 翻訳確認の為にMakefileに下記のコマンドを追記すると便利

<pre><code>$ vi Makefile</code></pre>

追記内容：
<pre><code>jmaster:
	make clean
	mkdir -p $(BUILDDIR)
	cd chef_master/scripts ; ruby po2mo.rb
	sphinx-build chef_master/source $(BUILDDIR)
</code></pre>

moを更新して、htmlを生成：
<pre><code>$ make jmaster</code></pre>


## 6. localeの中身のみをcommit & push

##### 【注】同時に、Progress.mdの更新もお願いします！！
<pre><code>$ vi pregoress.md</code></pre>

commit & push:
<pre><code>$ cd chef_master/locale
$ git commit -a -m "messages" # チャンとコミットメッセージを書いてください〜。
$ git push
</code></pre>