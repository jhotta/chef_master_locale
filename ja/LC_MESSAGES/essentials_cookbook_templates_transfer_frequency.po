# Japanese translations for Chef Docs package.
# Copyright (C) This work is licensed under a Creative Commons Attribution 3.0 Unported License
# This file is distributed under the same license as the Chef Docs package.
# Automatically generated, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: Chef Docs 0.0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-04-15 09:31\n"
"PO-Revision-Date: 2013-04-15 11:04+0900\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 1.5.5\n"

# c48b965adaf14dcaa6ef6c1999ae7222
#: ../../chef_master/source/essentials_cookbook_templates_transfer_frequency.rst:34
msgid "Transfer Frequency"
msgstr ""

# f976e0f794ce489a8381947c4d56f3bb
#: ../../includes_cookbooks/includes_cookbooks_template_transfer_frequency.rst:4
msgid "|chef| caches a template when it is first requested. On each subsequent request for that template, |chef| compares that request to the template located on the |chef server|. If the templates are the same, no transfer occurs."
msgstr ""

