# Japanese translations for Chef Docs package.
# Copyright (C) This work is licensed under a Creative Commons Attribution 3.0 Unported License
# This file is distributed under the same license as the Chef Docs package.
# Automatically generated, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: Chef Docs 0.0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-04-15 09:31\n"
"PO-Revision-Date: 2013-04-15 11:04+0900\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 1.5.5\n"

# d6d9f7fc63214daf87d446fcff514f2e
#: ../../chef_master/source/essentials_cookbook_lwrp_resources_common_actions.rst:34
msgid "Actions"
msgstr ""

# 004301d4bf1348ea8a259d146be62431
#: ../../includes_cookbooks/includes_cookbooks_resource_common_actions.rst:4
msgid "The following actions are common to every resource:"
msgstr ""

# d620d97c4d514d449fb1c9f06f677649
#: ../../includes_cookbooks/includes_cookbooks_resource_common_actions.rst:10
msgid "Action"
msgstr ""

# fec4e75003684db9ac34fc9216849a3c
#: ../../includes_cookbooks/includes_cookbooks_resource_common_actions.rst:11
msgid "Description"
msgstr ""

# 407ba89e49714e89a27a16aadb0888ec
#: ../../includes_cookbooks/includes_cookbooks_resource_common_actions.rst:12
msgid "``:nothing``"
msgstr ""

# 1817cf768ab649d7939c873299e5b3e3
#: ../../includes_cookbooks/includes_cookbooks_resource_common_actions.rst:13
msgid "Use to do nothing. In the absence of another default action, ``nothing`` is the default. This action can be useful to specify a resource so that it can be notified of other actions."
msgstr ""

