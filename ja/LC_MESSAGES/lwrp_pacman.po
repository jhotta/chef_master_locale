# Japanese translations for Chef Docs package.
# Copyright (C) This work is licensed under a Creative Commons Attribution 3.0 Unported License
# This file is distributed under the same license as the Chef Docs package.
# Automatically generated, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: Chef Docs 0.0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-04-15 09:31\n"
"PO-Revision-Date: 2013-04-15 11:04+0900\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 1.5.5\n"

# dbd9de35cca84785b99c8ac0579fcb31
#: ../../chef_master/source/lwrp_pacman.rst:34
msgid "pacman Lightweight Resource"
msgstr ""

# dc01b896758e476f8577b8cf54de50fd
#: ../../chef_master/source/lwrp_pacman.rst:36
msgid "|cookbook name pacman|"
msgstr ""

# e19d833c590d454593e42e02b38336b9
#: ../../chef_master/source/lwrp_pacman.rst:38
msgid "The ``pacman`` cookbook contains the following lightweight resources: ``pacman_aur`` and ``pacman_group``."
msgstr ""

# b4cc85cf7c0541ca80b6ed4104b564c1
#: ../../chef_master/source/lwrp_pacman.rst:41
msgid "pacman_aur"
msgstr ""

# a39df4b6a3d8497683621291da81de46
# ae55930244d646bc9c1dfb165f1c484a
#: ../../includes_lwrp/includes_lwrp_pacman_aur.rst:4
msgid "The |lwrp pacman aur| lightweight resource is used to use the |archlinux| package manager (|archlinux pacman|). A custom package description file (``PKGBUILD``) can be used when creating packages."
msgstr ""

# 635a816517ec45468a5225a53a79c947
# ca35b6c7966a4629b44f531c5bb6748b
#: ../../includes_lwrp/includes_lwrp_pacman_aur.rst:6
msgid "For more information, see: https://wiki.archlinux.org/index.php/PKGBUILD."
msgstr ""

# 39e87872d989499a823ed092f05695dc
# 720e356995d54e0f839002b608c7a473
#: ../../chef_master/source/lwrp_pacman.rst:44
#: ../../chef_master/source/lwrp_pacman.rst:64
msgid "This lightweight resource is part of the ``pacman`` cookbook (http://community.opscode.com/cookbooks/pacman)."
msgstr ""

# 3dad77885a0144f38399b2b4d40e53c5
# 97ef1a3a764c4d30a247fdb52011a8db
#: ../../chef_master/source/lwrp_pacman.rst:47
#: ../../chef_master/source/lwrp_pacman.rst:67
msgid "Actions"
msgstr ""

# d1652b65ccfa40f0bde4019956889d21
# 019d9ae22ae0421a82ec0dd447defb2b
#: ../../includes_lwrp/includes_lwrp_pacman_aur_actions.rst:4
#: ../../includes_lwrp/includes_lwrp_pacman_aur_attributes.rst:4
#: ../../includes_lwrp/includes_lwrp_pacman_group_actions.rst:4
msgid "This lightweight resource provider has the following actions:"
msgstr ""

# abcb20838e4446fcb06fa3a3d2c7afa2
# 644416be53f948cf94cb578728ce7300
#: ../../includes_lwrp/includes_lwrp_pacman_aur_actions.rst:10
#: ../../includes_lwrp/includes_lwrp_pacman_aur_attributes.rst:10
#: ../../includes_lwrp/includes_lwrp_pacman_group_actions.rst:10
msgid "Action"
msgstr ""

# 258ba58bcfed4ee39ffff3e2b407f0e7
# 354ed05c3d8f49539cf1ce7ca3cababe
# 99e0ca44df174863bbc806b7597a1dfd
#: ../../includes_lwrp/includes_lwrp_pacman_aur_actions.rst:11
#: ../../includes_lwrp/includes_lwrp_pacman_aur_attributes.rst:11
#: ../../includes_lwrp/includes_lwrp_pacman_group_actions.rst:11
#: ../../includes_lwrp/includes_lwrp_pacman_group_attributes.rst:11
msgid "Description"
msgstr ""

# a5529a1c1e944fba9feb0e294c200737
#: ../../includes_lwrp/includes_lwrp_pacman_aur_actions.rst:12
msgid "``:build``"
msgstr ""

# 537ba6a979af42b69c63f526634af42a
#: ../../includes_lwrp/includes_lwrp_pacman_aur_actions.rst:13
msgid "|lwrp action build pacman_aur|"
msgstr ""

# 88d48df262e449bba800a84328b6ef8e
#: ../../includes_lwrp/includes_lwrp_pacman_aur_actions.rst:14
#: ../../includes_lwrp/includes_lwrp_pacman_group_actions.rst:12
msgid "``:install``"
msgstr ""

# 6c700a7f092d4062a389df9b03b1c247
#: ../../includes_lwrp/includes_lwrp_pacman_aur_actions.rst:15
msgid "|lwrp action install pacman_aur|"
msgstr ""

# 21598e87278b49e2a56730c5709aa7e0
# cfea50b66b6b41c2967a3a028d238671
#: ../../chef_master/source/lwrp_pacman.rst:51
#: ../../chef_master/source/lwrp_pacman.rst:71
msgid "Attributes"
msgstr ""

# 1d9d64caa5334b869bd405b5fe73db9d
#: ../../includes_lwrp/includes_lwrp_pacman_aur_attributes.rst:12
msgid "``builddir``"
msgstr ""

# ac28c9b5c04c432d90ad7ef453db0c1b
#: ../../includes_lwrp/includes_lwrp_pacman_aur_attributes.rst:13
msgid "|builddir lwrp pacman_aur|"
msgstr ""

# aaf97f35225d48d19be7c002033b3a6b
#: ../../includes_lwrp/includes_lwrp_pacman_aur_attributes.rst:14
msgid "``exists``"
msgstr ""

# b2fc32e86eca4752b23e4a7395a2b546
#: ../../includes_lwrp/includes_lwrp_pacman_aur_attributes.rst:15
msgid "|exists lwrp pacman_aur|"
msgstr ""

# ad088b3867ab4428ba745abe3b8c4c3e
#: ../../includes_lwrp/includes_lwrp_pacman_aur_attributes.rst:16
#: ../../includes_lwrp/includes_lwrp_pacman_group_attributes.rst:14
msgid "``options``"
msgstr ""

# 8a7b0585fa0c49c6811cedbc9b070465
#: ../../includes_lwrp/includes_lwrp_pacman_aur_attributes.rst:17
msgid "|options lwrp pacman_aur|"
msgstr ""

# 6badc51249bc47eba6f771a8b27f05e4
#: ../../includes_lwrp/includes_lwrp_pacman_aur_attributes.rst:18
#: ../../includes_lwrp/includes_lwrp_pacman_group_attributes.rst:12
msgid "``package_name``"
msgstr ""

# 0dc503d4f98444cf867d7cbb45fd2667
#: ../../includes_lwrp/includes_lwrp_pacman_aur_attributes.rst:19
msgid "|package_name lwrp pacman_aur|"
msgstr ""

# 635ec96532684bfabef96f2a9e18fddd
#: ../../includes_lwrp/includes_lwrp_pacman_aur_attributes.rst:20
msgid "``patches``"
msgstr ""

# d04e9c8ebdbf4242a2b5f5c43681096e
#: ../../includes_lwrp/includes_lwrp_pacman_aur_attributes.rst:21
msgid "|patches lwrp pacman_aur|"
msgstr ""

# 681644627a54432484086c5d9fdc2c54
#: ../../includes_lwrp/includes_lwrp_pacman_aur_attributes.rst:22
msgid "``pkgbuild_src``"
msgstr ""

# 39e41fe4e7d1470db30f4ff4829becae
#: ../../includes_lwrp/includes_lwrp_pacman_aur_attributes.rst:23
msgid "|pkgbuild_src lwrp pacman_aur|"
msgstr ""

# 5326a63785a4453c933195572807cfca
#: ../../includes_lwrp/includes_lwrp_pacman_aur_attributes.rst:24
msgid "``version``"
msgstr ""

# 74ae2bae285c46c5baa600b842398cf1
#: ../../includes_lwrp/includes_lwrp_pacman_aur_attributes.rst:25
msgid "|version lwrp pacman_aur|"
msgstr ""

# daf42f5300744c6da5066ec153e7da4f
# c546db994cef45d68ee20914b919fe12
#: ../../chef_master/source/lwrp_pacman.rst:55
#: ../../chef_master/source/lwrp_pacman.rst:75
msgid "Examples"
msgstr ""

# 49024dd8962d4c3e94fd3aa4b9c72f70
#: ../../step_lwrp/step_lwrp_pacman_aur_use_simple_package.rst:3
msgid "To use a simple AUR package"
msgstr ""

# fd826ba8bf384cd1b0c9218677493377
#: ../../step_lwrp/step_lwrp_pacman_aur_use_custom_package.rst:3
msgid "To use a custom package with PKGBUILD"
msgstr ""

# 30964249fac14ece9617dbef347e353e
#: ../../chef_master/source/lwrp_pacman.rst:61
msgid "pacman_group"
msgstr ""

# 142d7b3ff74a4e48895d5ff6c67031ff
#: ../../includes_lwrp/includes_lwrp_pacman_group.rst:4
msgid "The |lwrp pacman group| lightweight resource is used to manage a |archlinux pacman| package group."
msgstr ""

# 1dea8c091dbf47c3a7f1e9cd72d8d835
#: ../../includes_lwrp/includes_lwrp_pacman_group_actions.rst:13
msgid "|lwrp action install pacman_group|"
msgstr ""

# 3ca28e9eba0a419599adb191d01f3709
#: ../../includes_lwrp/includes_lwrp_pacman_group_actions.rst:14
msgid "``:remove``"
msgstr ""

# 8f404c8483eb49b39847fd136177f0ba
#: ../../includes_lwrp/includes_lwrp_pacman_group_actions.rst:15
msgid "|lwrp action remove pacman_group|"
msgstr ""

# 72cfcfbf3ace49d0b04dab353e91be80
#: ../../includes_lwrp/includes_lwrp_pacman_group_attributes.rst:4
msgid "This lightweight resource provider has the following attributes:"
msgstr ""

# aafc44be6a8e4449b85fac865f45d821
#: ../../includes_lwrp/includes_lwrp_pacman_group_attributes.rst:10
msgid "Attribute"
msgstr ""

# e7e9612bdcb246dc9148607646c9406b
# 6c923ba6919e406eb500b0ca46885ed6
#: ../../includes_lwrp/includes_lwrp_pacman_group_attributes.rst:13
#: ../../includes_lwrp/includes_lwrp_pacman_group_attributes.rst:15
msgid "|package_name lwrp pacman_group|"
msgstr ""

# baf0661034ec4042854817b4c2e7e2d7
#: ../../step_lwrp/step_lwrp_pacman_group_base_devel.rst:3
msgid "To remove the \"base-devel\" group:"
msgstr ""

