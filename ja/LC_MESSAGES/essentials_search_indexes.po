# Japanese translations for Chef Docs package.
# Copyright (C) This work is licensed under a Creative Commons Attribution 3.0 Unported License
# This file is distributed under the same license as the Chef Docs package.
# Automatically generated, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: Chef Docs 0.0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-04-15 09:31\n"
"PO-Revision-Date: 2013-04-15 11:04+0900\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 1.5.5\n"

# 0e3625a7a7f3400c8665e8f43e39da53
#: ../../chef_master/source/essentials_search_indexes.rst:34
msgid "Search Indexes"
msgstr ""

# dfd1d14d543c4b8dbe54a3173def386e
#: ../../includes_search/includes_search_index.rst:5
msgid "|search index| |chef| builds the following search indexes:"
msgstr ""

# 3d75deb1a72b4328a170ac302cf30270
#: ../../includes_search/includes_search_index.rst:11
msgid "Search Index Name"
msgstr ""

# 855a87c058104da49dfbf431864e9512
#: ../../includes_search/includes_search_index.rst:12
msgid "Description"
msgstr ""

# b11c15b7eb464d0ca6f3895de7bd1d3c
#: ../../includes_search/includes_search_index.rst:13
msgid "``client``"
msgstr ""

# 75d782ce24a84910b0d517b10e1119e8
#: ../../includes_search/includes_search_index.rst:14
msgid "|chef api client|"
msgstr ""

# e22111a8b638423c9d7cec5c1568da73
#: ../../includes_search/includes_search_index.rst:15
msgid "``DATA_BAG_NAME``"
msgstr ""

# 51c5df9318484455beab96e48ccd963e
#: ../../includes_search/includes_search_index.rst:16
msgid "|data bag description| The name of the search index is the name of the data bag. For example, if the name of the data bag was \"admins\" then a corresponding search query might look something like ``search(:admins, \"*:*\")``."
msgstr ""

# c5e54663df3a4c6392f4b325e762d629
#: ../../includes_search/includes_search_index.rst:17
msgid "``environment``"
msgstr ""

# ef29ae2d3eb746f3a981c42d19c0bbf1
#: ../../includes_search/includes_search_index.rst:18
msgid "|environment description|"
msgstr ""

# dde5fcc0ac354118a212c7d34539ae82
#: ../../includes_search/includes_search_index.rst:19
msgid "``node``"
msgstr ""

# f65cba6d8fbb4196abd89f70c6168766
#: ../../includes_search/includes_search_index.rst:20
msgid "|node description|"
msgstr ""

# 4dadd737417143b3806e28ac70eb6362
#: ../../includes_search/includes_search_index.rst:21
msgid "``role``"
msgstr ""

# 2480a7a86a2843268ed951092b18dba5
#: ../../includes_search/includes_search_index.rst:22
msgid "|role description|"
msgstr ""

# f97d04ec12dc4bc69d1fcc45953dcb42
#: ../../chef_master/source/essentials_search_indexes.rst:39
msgid "Using Knife to Search"
msgstr ""

# 8824e5e647e14f85986a30e674125720
#: ../../includes_knife/includes_knife_search.rst:6
msgid "|knife search|"
msgstr ""

# db1d2e794a604b028b3475c875d93a70
#: ../../includes_knife/includes_knife_search_examples.rst:6
msgid "For example, to search for the IDs of all nodes running on the |amazon ec2| platform, enter:"
msgstr ""

# c89adfbb714344839e8ec799666ab450
# a3c1cf28c7f04392ad6b96639b90cc90
#: ../../includes_knife/includes_knife_search_examples.rst:12
#: ../../includes_knife/includes_knife_search_examples.rst:32
msgid "to return something like:"
msgstr ""

# 6f74bafef36f423e82c5a858aa6265c0
#: ../../includes_knife/includes_knife_search_examples.rst:26
msgid "To search for the instance type (flavor) of all nodes running on the |amazon ec2| platform, enter:"
msgstr ""

# 55c2dbb9378b49b0800ee0bbfa0fd3fc
#: ../../includes_knife/includes_knife_search_examples.rst:50
msgid "To search for all nodes running |ubuntu|, enter:"
msgstr ""

# 12a1c968480c4f028cc7f021f23570c1
#: ../../includes_knife/includes_knife_search_examples.rst:56
msgid "To search for all nodes running |centos| in the production environment, enter:"
msgstr ""

# e8b064f9a0f6431c97e9206b0ca0dd9e
#: ../../includes_knife/includes_knife_search_examples.rst:62
msgid "To find a nested attribute, use a pattern similar to the following:"
msgstr ""

# a35fd08697474cf1946286f8d9fa0c30
#: ../../includes_knife/includes_knife_search_examples.rst:68
msgid "To build a search query that can find a nested attribute:"
msgstr ""

# 4ecb73b56cb74709984f0e8d8b6ad5a5
#: ../../includes_knife/includes_knife_search_examples.rst:74
msgid "To test a search query that will be used in a ``knife ssh`` command:"
msgstr ""

# 2d46521e4003484aaff2d0e3fcfc5bed
#: ../../includes_knife/includes_knife_search_examples.rst:80
msgid "where the query in the previous example will search all servers that have the ``web`` role, but not on the server named ``web03``."
msgstr ""

