# Japanese translations for Chef Docs package.
# Copyright (C) This work is licensed under a Creative Commons Attribution 3.0 Unported License
# This file is distributed under the same license as the Chef Docs package.
# Automatically generated, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: Chef Docs 0.0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-04-15 09:31\n"
"PO-Revision-Date: 2013-04-15 11:04+0900\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 1.5.5\n"

# 9291c6a1529d4af8b77ae95a168c6272
#: ../../chef_master/source/essentials_cookbook_recipes.rst:34
msgid "About Recipes"
msgstr ""

# d773a47041fa4fca955096b1dd61e488
#: ../../includes_cookbooks/includes_cookbooks_recipe.rst:4
msgid "A recipe is the most fundamental configuration element within the |chef| environment. A recipe:"
msgstr ""

# 885a9837836e471198fb98ededa40abe
#: ../../includes_cookbooks/includes_cookbooks_recipe.rst:6
msgid "Is authored using |ruby|, which is a programming language designed to read and behave in a predictable manner"
msgstr ""

# 17fce66e52cd4c68bfc6c413ce191477
#: ../../includes_cookbooks/includes_cookbooks_recipe.rst:7
msgid "Is mostly a collection of resources in a |ruby| syntax with some helper code around it"
msgstr ""

# 3aef1c32b6704d88be1760bc046b4638
#: ../../includes_cookbooks/includes_cookbooks_recipe.rst:8
msgid "Must define everything that is required to configure part of a system"
msgstr ""

# 35b72e166ed5408f9252caa794d214f1
#: ../../includes_cookbooks/includes_cookbooks_recipe.rst:9
msgid "Must be stored in a cookbook"
msgstr ""

# 2dd92730c9484b83be94a2bdce005e49
#: ../../includes_cookbooks/includes_cookbooks_recipe.rst:10
msgid "May be included in a recipe"
msgstr ""

# 6fd4aaf0b78145c48ed9a8ece701f581
#: ../../includes_cookbooks/includes_cookbooks_recipe.rst:11
msgid "May use the results of a search query and read the contents of a data bag (including an encrypted data bag)"
msgstr ""

# b4bd94c9a1a04149a04019f45ad04920
#: ../../includes_cookbooks/includes_cookbooks_recipe.rst:12
msgid "May have a dependency on one (or more) recipes"
msgstr ""

# 730e9071d5f741a687842cb041d1e0f6
#: ../../includes_cookbooks/includes_cookbooks_recipe.rst:13
msgid "May be tagged to facilitate the creation of arbitrary groupings that exist outside of the normal naming conventions an organization may have"
msgstr ""

# 1702de4cff5f4e5ebcadc5a9ddd8d6b3
#: ../../includes_cookbooks/includes_cookbooks_recipe.rst:14
msgid "Must be added to a run-list before it can be used by |chef|"
msgstr ""

# e1928d731cdc4b2d9ba74327c321e6e2
#: ../../includes_cookbooks/includes_cookbooks_recipe.rst:15
msgid "Is always executed in the same order as listed in a run-list"
msgstr ""

# 5cf7059b9a464bccb96daa00d70ead8c
# 40a7451d97cf45949c78cb42e3e871f3
#: ../../chef_master/source/essentials_cookbook_recipes.rst:42
#: ../../chef_master/source/essentials_cookbook_recipes.rst:69
msgid "Topic"
msgstr ""

# 6267ac2794ba4c7599439583acdb6e5b
# 72f44c13f603463fa59a40c73ab033c5
#: ../../chef_master/source/essentials_cookbook_recipes.rst:43
#: ../../chef_master/source/essentials_cookbook_recipes.rst:70
msgid "Description"
msgstr ""

# 83a1b241425e41e9b8fd7b4d7806999f
#: ../../chef_master/source/essentials_cookbook_recipes.rst:44
msgid ":doc:`essentials_cookbook_recipes_data_bags`"
msgstr ""

# 606bd0b2bc414bb9867ef57fe32cbdea
#: ../../chef_master/source/essentials_cookbook_recipes.rst:45
msgid "A data bag can be loaded by a recipe."
msgstr ""

# 6af21b11c4f144bd975916ae6e754b20
#: ../../chef_master/source/essentials_cookbook_recipes.rst:46
msgid ":doc:`essentials_cookbook_recipes_search`"
msgstr ""

# 49eabb0a92554b3d9224af2d8adb16e1
#: ../../chef_master/source/essentials_cookbook_recipes.rst:47
msgid "A search is a full-text query that can be done from several locations, including from within a recipe."
msgstr ""

# ecc823bcf282412e893f093bbcdde11b
#: ../../chef_master/source/essentials_cookbook_recipes.rst:48
msgid ":doc:`essentials_cookbook_recipes_use_ruby`"
msgstr ""

# ee7b2d9918ac4c58b86c7b473bc77f4f
#: ../../chef_master/source/essentials_cookbook_recipes.rst:49
msgid "Anything that can be done with Ruby can be used within a recipe."
msgstr ""

# fe534fc1b0134729892e2d146ff28dd9
#: ../../chef_master/source/essentials_cookbook_recipes.rst:50
msgid ":doc:`essentials_cookbook_recipes_in_recipes`"
msgstr ""

# e367cb45aa9c43e3a71fac92cef302ba
#: ../../chef_master/source/essentials_cookbook_recipes.rst:51
msgid "A recipe can include one (or more) recipes found in other cookbooks by using the ``include_recipe`` keyword."
msgstr ""

# 30c78f8dba924164b32356d9f567971a
#: ../../chef_master/source/essentials_cookbook_recipes.rst:52
msgid ":doc:`essentials_cookbook_recipes_run_lists`"
msgstr ""

# 83c93485a34c4a15ac77f66357253550
#: ../../chef_master/source/essentials_cookbook_recipes.rst:53
msgid "A recipe must be assigned to a run-list using the appropriate name, as defined by the cookbook directory and namespace."
msgstr ""

# b26024c6ebe84cebaca8eef26092a91f
#: ../../chef_master/source/essentials_cookbook_recipes.rst:54
msgid ":doc:`essentials_cookbook_recipes_cookbook_dependencies`"
msgstr ""

# eb39e55f876f4392b19017c3017779fa
#: ../../chef_master/source/essentials_cookbook_recipes.rst:55
msgid "If a cookbook has a dependency on a recipe that is located in another cookbook, that dependency must be declared in the |metadata rb| file for that cookbook using the ``depends`` keyword."
msgstr ""

# d1479f1124074626b714ebd270759e79
#: ../../chef_master/source/essentials_cookbook_recipes.rst:56
msgid ":doc:`essentials_cookbook_recipes_handlers_and_logs`"
msgstr ""

# 1ee6855898b449988f83b87b49a89b89
#: ../../chef_master/source/essentials_cookbook_recipes.rst:57
msgid "A recipe can write events to a |chef| log file and can cause exceptions using ``Chef::Log``."
msgstr ""

# 5bfe602235374ce087947afe246cfcf0
#: ../../chef_master/source/essentials_cookbook_recipes.rst:58
msgid ":doc:`essentials_cookbook_recipes_tags`"
msgstr ""

# 87d595446c724d099d30fa9be565ad22
#: ../../chef_master/source/essentials_cookbook_recipes.rst:59
msgid "A tag is a custom description that is applied to a node that can be helpful when building recipes by providing alternate methods of grouping similar types of information."
msgstr ""

# d47fdb31c63d42fbad4a9aeb6960a35c
#: ../../chef_master/source/essentials_cookbook_recipes.rst:60
msgid ":doc:`essentials_cookbook_recipes_end_chef_run`"
msgstr ""

# 705578dfec82446bbafb03cbf2612166
#: ../../chef_master/source/essentials_cookbook_recipes.rst:61
msgid "Sometimes a |chef| run needs to be stopped. There are various ways for doing that from within a recipe."
msgstr ""

# 3a3d2aa69e864ffb9456ddb3f3ad0bd8
#: ../../includes_cookbooks/includes_cookbooks_recipe_attribute.rst:4
msgid "An attribute can be defined in a recipe and then used to override the default settings on a node. When a recipe is applied during a |chef| run, these attributes are compared to the attributes that are already present on the node. When the recipe attributes take precedence over the default attributes, |chef| will apply those new settings and values during the |chef| run."
msgstr ""

# f4a142ec7d7049279a3d33777807b957
#: ../../chef_master/source/essentials_cookbook_recipes.rst:71
msgid ":doc:`essentials_cookbook_recipes_attribute_types`"
msgstr ""

# 3fa4ff3b61704645b0f598958dcc0635
#: ../../chef_master/source/essentials_cookbook_recipes.rst:72
msgid "There are four types of attributes: ``default``, ``normal``, ``override``, and ``automatic``. Both ``default`` and ``override`` attributes can be forced on a case-by-case basis."
msgstr ""

# 22926504ac3b4c9b90ddad6edc1449c6
#: ../../chef_master/source/essentials_cookbook_recipes.rst:73
msgid ":doc:`essentials_cookbook_recipes_attribute_persistence`"
msgstr ""

# 1d24c34d98b74e7da2dab61ee9072e8a
#: ../../chef_master/source/essentials_cookbook_recipes.rst:74
msgid "During a |chef| run, saved attributes are retrieved from the |chef server| and are merged with the attributes on the local system."
msgstr ""

# db4523921d574de4a32388be6ba820e2
#: ../../chef_master/source/essentials_cookbook_recipes.rst:75
msgid ":doc:`essentials_cookbook_recipes_attribute_precedence`"
msgstr ""

# 0c566af617994fb29fe21ae95415dd3a
#: ../../chef_master/source/essentials_cookbook_recipes.rst:76
msgid "At the beginning of a |chef| run, all default, override, and automatic attributes are reset."
msgstr ""

# bffcc9f99b7e4c0499d6719f353f3378
#: ../../chef_master/source/essentials_cookbook_recipes.rst:77
msgid ":doc:`essentials_cookbook_recipes_attribute_automatic`"
msgstr ""

# f3c3b594f38448798b23899223bba7f8
#: ../../chef_master/source/essentials_cookbook_recipes.rst:78
msgid "An automatic attribute is data that must be understood by |chef|, but not modified."
msgstr ""

# 3ce81218d3cd4d87a226e0accc465fa5
#: ../../chef_master/source/essentials_cookbook_recipes.rst:79
msgid ":doc:`essentials_cookbook_recipes_attribute_notation`"
msgstr ""

# f8c834b96eab4ff29ed7e7029427e320
#: ../../chef_master/source/essentials_cookbook_recipes.rst:80
msgid "Attributes are a special key-value store called a mash within the context of the |ruby| DSL."
msgstr ""

# 7d980a00f0184bf49a04ea1b729feea3
#: ../../chef_master/source/essentials_cookbook_recipes.rst:81
msgid ":doc:`essentials_cookbook_recipes_attribute_methods`"
msgstr ""

# 4bdea69906e74cebae98949973e1b1c6
#: ../../chef_master/source/essentials_cookbook_recipes.rst:82
msgid "Methods are available to set attribute precedence in a cookbook."
msgstr ""

# e7e87b384ca04c4aa0f258eb6c494442
#: ../../chef_master/source/essentials_cookbook_recipes.rst:83
msgid ":doc:`essentials_cookbook_recipes_environment_variables`"
msgstr ""

# ba850450d33f4d309e39c591248e0b13
#: ../../chef_master/source/essentials_cookbook_recipes.rst:84
msgid "|unix| environment variables can be used in recipes."
msgstr ""

