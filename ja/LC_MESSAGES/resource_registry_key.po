# Japanese translations for Chef Docs package.
# Copyright (C) This work is licensed under a Creative Commons Attribution 3.0 Unported License
# This file is distributed under the same license as the Chef Docs package.
# Automatically generated, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: Chef Docs 0.0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-04-15 09:31\n"
"PO-Revision-Date: 2013-04-15 11:04+0900\n"
"Last-Translator: Naotaka Jay Hotta <fukuzo@cybertron.co.jp>\n"
"Language-Team: none\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 1.5.5\n"

# 6a8459dcfee64ccf89ed88bd6c362cfc
#: ../../chef_master/source/resource_registry_key.rst:34
msgid "registry_key"
msgstr ""

# c5863ed3e77f4fed85aa4d2a73e46661
#: ../../includes_resources/includes_resource_generic.rst:4
msgid "A `resource <http://docs.opscode.com/resource.html>`_ is a key part of a `recipe <http://docs.opscode.com/essentials_cookbook_recipes.html>`_ that defines the actions that can be taken against a piece of the system. These actions are identified during each `Chef run <http://docs.opscode.com/essentials_nodes_chef_run.html>`_ as the resource collection is compiled. Once identified, each resource (in turn) is mapped to a provider, which then configures each piece of the system."
msgstr ""

# 2ebd7c9ef5af42dca3ad2b8da346ba54
#: ../../includes_resources/includes_resource_registry_key.rst:4
#, fuzzy
msgid "|resource desc registry_key|"
msgstr "|resource desc windows_registry|"

# 71f0d9e1419042c0802b6b35b32e6c41
#: ../../includes_resources/includes_resource_registry_key.rst:6
msgid "64-bit versions of |windows| have a 32-bit compatibility layer in the registry that reflects and re-directs certain keys (and their sub-keys) into specific locations. By default, the registry functionality in |chef| will default to the machine architecture of the system that is being configured. |chef| can access any reflected or re-directed registry key. |chef| can write to any 64-bit registry location. (This behavior is not affected by |chef| running as a 32-bit application.) For more information, see: |http resource registry_key msdn|."
msgstr ""

# c5fd6a17dbeb46308a17ac2f00c27f54
#: ../../chef_master/source/resource_registry_key.rst:41
msgid "Syntax"
msgstr ""

# 0cceab157c9f43e5bee177fb1aa8be89
#: ../../includes_resources/includes_resource_registry_key_syntax.rst:4
msgid "The syntax for using the |resource registry_key| resource in a recipe is as follows:"
msgstr ""

# ab08aa9067004eae894c5c45cc7d2ed3
#: ../../includes_resources/includes_resource_registry_key_syntax.rst:26
msgid "where"
msgstr ""

# 2b407b453d9c4d9f84e4f33ef92fea85
#: ../../includes_resources/includes_resource_registry_key_syntax.rst:28
msgid "``registry_key`` tells |chef| to use the ``Chef::Provider::Windows::Registry`` provider during the |chef| run"
msgstr ""

# 005e9d2d7abe418ea9d23d79cb03406a
#: ../../includes_resources/includes_resource_registry_key_syntax.rst:29
msgid "``\"name\"`` is the path to the registry"
msgstr ""

# 4a9243c204d948f7881dc7685df78d27
#: ../../includes_resources/includes_resource_registry_key_syntax.rst:30
msgid "``attribute`` is zero (or more) of the attributes that are available for this resource"
msgstr ""

# 073de494b4044a54b8e6bb6e25e71c9e
#: ../../includes_resources/includes_resource_registry_key_syntax.rst:31
msgid "``values`` is a hash that contains at least one registry key to be created or deleted. Each registry key in the hash is grouped by brackets in which the ``:name``, ``:type``, and ``:data`` values for that registry key are specified."
msgstr ""

# 3dc68e77c37b4d6dbb65da70803c7d98
# b6da6b681af445fe988a15e74a52d39c
#: ../../includes_resources/includes_resource_registry_key_syntax.rst:32
#: ../../includes_resources/includes_resource_registry_key_attributes.rst:29
msgid "|values resource registry_key types|"
msgstr "|values resource registry_key types|"

# 7a634219356d4dbea88e9cff359dea81
#: ../../includes_resources/includes_resource_registry_key_syntax.rst:33
msgid "``:action`` is the step that the resource will ask the provider to take during the |chef| run"
msgstr ""

# 485b39629f9a496f891544d0180eca98
#: ../../includes_resources/includes_resource_registry_key_syntax.rst:35
msgid "The following is an example of how the |resource registry_key| resource can work when used in a recipe. In this example, a |windows| registry key named \"System\" will get a new value called \"NewRegistryKeyValue\" and a mulit-string value named \"foo bar\":"
msgstr ""

# 422b7e97b0f64e00aa81f9f9f22093e3
#: ../../chef_master/source/resource_registry_key.rst:45
msgid "Actions"
msgstr ""

# 0452d172e9f04561a967dc924bbefa07
#: ../../includes_resources/includes_resource_registry_key_actions.rst:4
msgid "This resource has the following actions:"
msgstr ""

# e3712f279299444aa6b29ca20de73ade
#: ../../includes_resources/includes_resource_registry_key_actions.rst:10
msgid "Action"
msgstr ""

# c7fc1d576878485db76a5adb9d4cf91c
# b53bee7757b74d129a4404a54080f425
#: ../../includes_resources/includes_resource_registry_key_actions.rst:11
#: ../../includes_resources/includes_resource_registry_key_attributes.rst:11
msgid "Description"
msgstr ""

# 8c7ae89681804772a3ba8c82a2294013
#: ../../includes_resources/includes_resource_registry_key_actions.rst:12
msgid "``:create``"
msgstr "``:create``"

# 83b467a61bdb48a3977c36399224b23f
#: ../../includes_resources/includes_resource_registry_key_actions.rst:13
#, fuzzy
msgid "Default. |resource action create registry_key|"
msgstr "Default. |resource action create windows_registry|"

# 4de15d8d9b1840fc92e0229f9ada6810
#: ../../includes_resources/includes_resource_registry_key_actions.rst:14
msgid "``:create_if_missing``"
msgstr "``:create_if_missing``"

# c4f04be13f774977a7329f8dfca13351
#: ../../includes_resources/includes_resource_registry_key_actions.rst:15
#, fuzzy
msgid "|resource action create_if_missing registry_key|"
msgstr "|resource action create_if_missing windows_registry|"

# 5d012d64401145d09e994be9a13e62d8
#: ../../includes_resources/includes_resource_registry_key_actions.rst:16
msgid "``:delete``"
msgstr "``:delete``"

# f7da8c19473c4ffbb47ae2095722dc33
#: ../../includes_resources/includes_resource_registry_key_actions.rst:17
#, fuzzy
msgid "|resource action delete registry_key|"
msgstr "|resource action delete windows_registry|"

# d5f8a9ce46af4b5d99886a516d305483
#: ../../includes_resources/includes_resource_registry_key_actions.rst:18
msgid "``:delete_key``"
msgstr "``:delete_key``"

# be17cd3090e14d8cbc66b35480ad48c8
#: ../../includes_resources/includes_resource_registry_key_actions.rst:19
#, fuzzy
msgid "|resource action delete key registry_key|"
msgstr "|resource action delete key windows_registry|"

# 58da015775034fb288072bb1edb330e8
# 6e07fe2752e54811bc3ccf53d57a97a7
# c8c4edb2bda24168b30f2b8cc234318e
#: ../../includes_resources/includes_resource_registry_key_actions.rst:21
#: ../../includes_resources/includes_resource_registry_key_attributes.rst:25
#: ../../step_resource/step_resource_registry_key_delete_recursively.rst:12
msgid "|note registry_key resource recursive|"
msgstr "|note registry_key resource recursive|"

# b664e94006e44cbea166733fecb072bb
#: ../../chef_master/source/resource_registry_key.rst:49
msgid "Attributes"
msgstr ""

# 814fc380cf954b76af08316c1ed61c97
#: ../../includes_resources/includes_resource_registry_key_attributes.rst:4
msgid "This resource has the following attributes:"
msgstr ""

# 2203b81bc11d424fbdfa4bfda4652614
#: ../../includes_resources/includes_resource_registry_key_attributes.rst:10
msgid "Attribute"
msgstr ""

# 6501dc3d07c141caaebbdc1639df9af2
#: ../../includes_resources/includes_resource_registry_key_attributes.rst:12
msgid "``architecture``"
msgstr "``architecture``"

# 424c29266720447c808c6fbcf34cfd55
#: ../../includes_resources/includes_resource_registry_key_attributes.rst:13
msgid "|architecture resource registry_key|"
msgstr "|architecture resource registry_key|"

# b2aa36b5adfe4842a6aa885f86722ccf
#: ../../includes_resources/includes_resource_registry_key_attributes.rst:15
msgid "|architecture resource registry_key machine|"
msgstr "|architecture resource registry_key machine|"

# 176b78b0e593403cb98db8b6407abb00
#: ../../includes_resources/includes_resource_registry_key_attributes.rst:17
msgid "|note registry_key architecture|"
msgstr "|note registry_key architecture|"

# bb9d789f72f94497b7358c9b496d42a2
#: ../../includes_resources/includes_resource_registry_key_attributes.rst:18
msgid "``key``"
msgstr "``key``"

# 401134fdf0aa44e09426772db4dd25a6
#: ../../includes_resources/includes_resource_registry_key_attributes.rst:19
msgid "|key_name resource registry_key| This attribute defaults to the ``name`` of the resource if not specified."
msgstr ""

# 2b9aaf75c57b4ca2b13b34394d57a8c4
#: ../../includes_resources/includes_resource_registry_key_attributes.rst:21
msgid "|key_name resource registry_key hives|"
msgstr "|key_name resource registry_key hives|"

# 0235614fe6c948239d2dee0d0dfba81f
#: ../../includes_resources/includes_resource_registry_key_attributes.rst:22
msgid "``recursive``"
msgstr "``recursive``"

# 903bd203cd78475ea64ccd54aaab691f
#: ../../includes_resources/includes_resource_registry_key_attributes.rst:23
msgid "|recursive resource registry_key|"
msgstr "|recursive resource registry_key|"

# a3dd78337acd4b609e63b13130e68e46
#: ../../includes_resources/includes_resource_registry_key_attributes.rst:26
msgid "``values``"
msgstr "``values``"

# dd1658cf3ef148a1b9f9eeb01962a4e8
#: ../../includes_resources/includes_resource_registry_key_attributes.rst:27
msgid "|values resource registry_key|"
msgstr "|values resource registry_key|"

# a3cca8c3df35458384080ba4449bfa25
#: ../../chef_master/source/resource_registry_key.rst:53
msgid "Providers"
msgstr ""

# 388781b366f647bba20cec504154f031
#: ../../includes_resources/includes_resource_registry_key_providers.rst:4
msgid "The following providers are available. Use the short name to use the provider in a recipe:"
msgstr ""

# fd429f0c68d2498d90130e4195b3a4cd
#: ../../includes_resources/includes_resource_registry_key_providers.rst:10
msgid "Long name"
msgstr ""

# cb45642f6d3141abb34811a6f6da3811
#: ../../includes_resources/includes_resource_registry_key_providers.rst:11
msgid "Short name"
msgstr ""

# 8c1794bc82764c4f8b8c47779a2f1657
#: ../../includes_resources/includes_resource_registry_key_providers.rst:12
msgid "Notes"
msgstr ""

# 0101f2ae78514a98b9684b253b9f3584
#: ../../includes_resources/includes_resource_registry_key_providers.rst:13
msgid "``Chef::Provider::Windows::Registry``"
msgstr "``Chef::Provider::Windows::Registry``"

# 623789e07654487699777388d9873fd5
#: ../../includes_resources/includes_resource_registry_key_providers.rst:14
msgid "``registry_key``"
msgstr "``registry_key``"

# e517c9671558496987d436f1db467033
#: ../../includes_resources/includes_resource_registry_key_providers.rst:15
msgid "The default provider for the |windows| platform."
msgstr ""

# 8c9e7d5ff7884779bd0f3ebd44f062c7
#: ../../chef_master/source/resource_registry_key.rst:57
msgid "Examples"
msgstr ""

# ceaa99cb4d254e9e86141c890403c88b
#: ../../chef_master/source/resource_registry_key.rst:58
msgid "|generic resource statement|"
msgstr "|generic resource statement|"

# d211d4d2e41842669c25a90c2b70a8af
#: ../../chef_master/source/resource_registry_key.rst:60
msgid "**Create a registry key**"
msgstr ""

# e492094da97947799bd6bee95716541b
#: ../../chef_master/source/resource_registry_key.rst:64
msgid "**Delete a registry key value**"
msgstr ""

# b6b54f702ae94410877af0a9f2b81587
#: ../../chef_master/source/resource_registry_key.rst:68
msgid "**Delete a registry key and its subkeys, recursively**"
msgstr ""

# ae31f61b854f43bd9c690ac426dd0f2f
#: ../../chef_master/source/resource_registry_key.rst:72
msgid "**Use re-directed keys**"
msgstr ""

# a38de030ff1e4607b5c55ff9e23ca57b
#: ../../step_resource/step_resource_registry_key_redirect.rst:3
msgid "In 64-bit versions of |windows|, ``HKEY_LOCAL_MACHINE\\SOFTWARE\\Example`` is a re-directed key. In the following examples, because ``HKEY_LOCAL_MACHINE\\SOFTWARE\\Example`` is a 32-bit key, the output will be \"Found 32-bit key\" if they are run on a version of |windows| that is 64-bit:"
msgstr ""

# 70a3c9a79085424ea16a93db38294bd8
# 0ccf19f8d126424d9c9d8c201aff7f27
# e7d4d6ca617343a9b74ccff170f43df9
#: ../../step_resource/step_resource_registry_key_redirect.rst:13
#: ../../step_resource/step_resource_registry_key_redirect.rst:23
#: ../../step_resource/step_resource_registry_key_redirect.rst:34
msgid "or:"
msgstr ""

