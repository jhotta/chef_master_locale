# Japanese translations for Chef Docs package.
# Copyright (C) This work is licensed under a Creative Commons Attribution 3.0 Unported License
# This file is distributed under the same license as the Chef Docs package.
# Automatically generated, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: Chef Docs 0.0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-04-15 09:31\n"
"PO-Revision-Date: 2013-04-15 11:04+0900\n"
"Last-Translator: Naotaka Jay Hotta <fukuzo@cybertron.co.jp>\n"
"Language-Team: none\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 1.5.5\n"

# 7c19abcbecc0433caebfeb02592e2ae9
#: ../../chef_master/source/resource_yum.rst:34
msgid "yum_package"
msgstr "yum_package"

# d9287cb971764893beed41ac635c25c7
#: ../../includes_resources/includes_resource_generic.rst:4
msgid "A `resource <http://docs.opscode.com/resource.html>`_ is a key part of a `recipe <http://docs.opscode.com/essentials_cookbook_recipes.html>`_ that defines the actions that can be taken against a piece of the system. These actions are identified during each `Chef run <http://docs.opscode.com/essentials_nodes_chef_run.html>`_ as the resource collection is compiled. Once identified, each resource (in turn) is mapped to a provider, which then configures each piece of the system."
msgstr ""

# 310d7208c69c4bec987681a175d3056c
#: ../../includes_resources/includes_resource_yum_package.rst:4
msgid "|resource desc package_yum| This resource is based on the `package <http://docs.opscode.com/resource_package.html>`_ resource."
msgstr ""

# a7fa1a8eb0224cee8f351a5cf755e2d1
#: ../../includes_resources/includes_resource_yum_package.rst:6
msgid "|note resource_based_on_package|"
msgstr ""

# eadb893b218b44b1a49628df66cf1651
#: ../../chef_master/source/resource_yum.rst:40
msgid "|note yum resource using file names|"
msgstr "|note yum resource using file names|"

# 50176c6f87a24910bf6c1a563ebc97e3
#: ../../chef_master/source/resource_yum.rst:43
msgid "Syntax"
msgstr ""

# d69cab55793c43b4b0e4ea578e9b61d7
#: ../../includes_resources/includes_resource_yum_package_syntax.rst:4
msgid "The syntax for using the |resource yum_package| resource in a recipe is as follows:"
msgstr ""

# f2f11e427f2a467f944653a58b6516cd
#: ../../includes_resources/includes_resource_yum_package_syntax.rst:14
msgid "where"
msgstr ""

# b5692f26869842bbbfc0ae94ebcbe98c
#: ../../includes_resources/includes_resource_yum_package_syntax.rst:16
msgid "``yum_package`` tells |chef| to use the ``Yum`` provider during the |chef| run"
msgstr ""

# 0b61a0d1263e4d83873eeab40333f095
#: ../../includes_resources/includes_resource_yum_package_syntax.rst:17
msgid "``\"name\"`` is the name of the package"
msgstr ""

# 390a305036044fb9989c369170dea5df
#: ../../includes_resources/includes_resource_yum_package_syntax.rst:18
msgid "``attribute`` is zero (or more) of the attributes that are available for this resource"
msgstr ""

# 23792151c83c427a8d116726b93e68f4
#: ../../includes_resources/includes_resource_yum_package_syntax.rst:19
msgid "``:action`` is the step that the resource will ask the provider to take during the |chef| run"
msgstr ""

# 27cb2b11eccf42e186a7af163719945e
#: ../../chef_master/source/resource_yum.rst:47
msgid "Actions"
msgstr ""

# efc86817608c482dbf6cfa06c092f2bf
#: ../../includes_resources/includes_resource_yum_package_actions.rst:4
msgid "This resource has the following actions:"
msgstr ""

# 5d2cf1b0059f484ea18c04a9f9c474e4
#: ../../includes_resources/includes_resource_yum_package_actions.rst:10
msgid "Action"
msgstr ""

# b5ca109e61f1495facd1e004ff55e0b5
# 782421df32384863b6b38c7c93b38481
#: ../../includes_resources/includes_resource_yum_package_actions.rst:11
#: ../../includes_resources/includes_resource_yum_package_attributes.rst:11
msgid "Description"
msgstr ""

# eaeea9bdeec14892b89e97f8dc86a3c9
#: ../../includes_resources/includes_resource_yum_package_actions.rst:12
msgid "``:install``"
msgstr "``:install``"

# f55ba390307e40e2bc54312ce3882b02
#: ../../includes_resources/includes_resource_yum_package_actions.rst:13
msgid "Default. |resource action install yum|"
msgstr ""

# 959877221a9b42fb8d25fbf776dc9c9c
#: ../../includes_resources/includes_resource_yum_package_actions.rst:14
msgid "``:upgrade``"
msgstr "``:upgrade``"

# 6368e4f4fa24486f9cc3c600a4da2ef2
#: ../../includes_resources/includes_resource_yum_package_actions.rst:15
msgid "|resource action upgrade yum|"
msgstr "|resource action upgrade yum|"

# 609a8e9bb6044fe787cce3d3bf79ad37
#: ../../includes_resources/includes_resource_yum_package_actions.rst:16
msgid "``:remove``"
msgstr "``:remove``"

# 89d7e90b75054354b704cde6de5bdd1c
#: ../../includes_resources/includes_resource_yum_package_actions.rst:17
msgid "|resource action remove yum|"
msgstr "|resource action remove yum|"

# fd889a6a68a349c6a67417b0cf104922
#: ../../includes_resources/includes_resource_yum_package_actions.rst:18
msgid "``:purge``"
msgstr ""

# 6368e4f4fa24486f9cc3c600a4da2ef2
#: ../../includes_resources/includes_resource_yum_package_actions.rst:19
#, fuzzy
msgid "|resource action purge package|"
msgstr "|resource action upgrade yum|"

# 025c7848195e4c049d3cc0639d18496d
#: ../../chef_master/source/resource_yum.rst:51
msgid "Attributes"
msgstr ""

# bd769a38681a4140b8f159ae6e8c65a5
#: ../../includes_resources/includes_resource_yum_package_attributes.rst:4
msgid "This resource has the following attributes:"
msgstr ""

# 1eb192bf64074a8ca26f3f8b7f446278
#: ../../includes_resources/includes_resource_yum_package_attributes.rst:10
msgid "Attribute"
msgstr ""

# 9ec949d521d8424fa39a1a8d137829dd
#: ../../includes_resources/includes_resource_yum_package_attributes.rst:12
msgid "``allow_downgrade``"
msgstr "``allow_downgrade``"

# 9ec949d521d8424fa39a1a8d137829dd
#: ../../includes_resources/includes_resource_yum_package_attributes.rst:13
#, fuzzy
msgid "|allow_downgrade|"
msgstr "``allow_downgrade``"

# 379a344ed0a1467b8ccee4145a460ae8
#: ../../includes_resources/includes_resource_yum_package_attributes.rst:14
msgid "``arch``"
msgstr "``arch``"

# b800bf0189ec44918ae3540bf0ae48a2
#: ../../includes_resources/includes_resource_yum_package_attributes.rst:15
msgid "|arch resource yum|"
msgstr "|arch resource yum|"

# 66e7332118074df0ad2d888cffe6db17
#: ../../includes_resources/includes_resource_yum_package_attributes.rst:16
msgid "``flush_cache``"
msgstr "``flush_cache``"

# 37e3c6dad99543cfbb42ef20f8bbd2ae
#: ../../includes_resources/includes_resource_yum_package_attributes.rst:17
msgid "|flush_cache resource yum|"
msgstr "|flush_cache resource yum|"

# 077f992e34244ba3b73e7a2b0b72065a
#: ../../includes_resources/includes_resource_yum_package_attributes.rst:18
msgid "``options``"
msgstr "``options``"

# 3d93a5c1d63f4eaf95bc149fc43ec5c8
#: ../../includes_resources/includes_resource_yum_package_attributes.rst:19
#, fuzzy
msgid "|options resource package|"
msgstr "|options resource yum|"

# 2bccccc0dfb7487d9247ddd3b5860262
#: ../../includes_resources/includes_resource_yum_package_attributes.rst:20
msgid "``package_name``"
msgstr "``package_name``"

# a2691ecd06fe44b896044dd0ca675280
#: ../../includes_resources/includes_resource_yum_package_attributes.rst:21
#, fuzzy
msgid "|package_name resource package|"
msgstr "|package_name resource yum|"

# 860dca4798454deb9e554c434b58107e
#: ../../includes_resources/includes_resource_yum_package_attributes.rst:22
msgid "``response_file``"
msgstr ""

# 51beaab7dc264c15a3ed3670294694c2
#: ../../includes_resources/includes_resource_yum_package_attributes.rst:23
#, fuzzy
msgid "|response_file resource package|"
msgstr "|generic resource statement|"

# 4c0cab9a639440c6960eb2f9b253c433
#: ../../includes_resources/includes_resource_yum_package_attributes.rst:24
msgid "``source``"
msgstr "``source``"

# d6c32086f9ea4fc9870557bfd793bea5
#: ../../includes_resources/includes_resource_yum_package_attributes.rst:25
#, fuzzy
msgid "|source resource package|"
msgstr "``source``"

# e946f018b4cb415094e464a82d482351
#: ../../includes_resources/includes_resource_yum_package_attributes.rst:26
msgid "``version``"
msgstr "``version``"

# 04f80624abb04b05a4f939eeee96bc5e
#: ../../includes_resources/includes_resource_yum_package_attributes.rst:27
#, fuzzy
msgid "|version resource package|"
msgstr "|version resource yum|"

# b81292e7b2d34fea8b1bbb7998d432e2
#: ../../chef_master/source/resource_yum.rst:55
msgid "Providers"
msgstr ""

# ca0853e3fad74ffc9dedfddac93f326c
#: ../../includes_resources/includes_resource_yum_package_providers.rst:4
msgid "This resource does not have any providers."
msgstr ""

# c01f229868164bf7ad1ce857bc5687c0
#: ../../chef_master/source/resource_yum.rst:59
msgid "Examples"
msgstr ""

# 51beaab7dc264c15a3ed3670294694c2
#: ../../chef_master/source/resource_yum.rst:60
msgid "|generic resource statement|"
msgstr "|generic resource statement|"

# 94ea2ee645a84c0e88073289ba16208f
#: ../../chef_master/source/resource_yum.rst:62
msgid "**Install an exact version**"
msgstr ""

# 55945eda21eb47649388532a9b94c73d
#: ../../chef_master/source/resource_yum.rst:66
msgid "**Install a minimum version**"
msgstr ""

# ab980b4c8ebf4976b1cf448d6e9f6b33
#: ../../chef_master/source/resource_yum.rst:70
msgid "**Install a minimum version using the default action**"
msgstr ""

# 70d62f08cfeb490ea4afe8decb834f5d
#: ../../chef_master/source/resource_yum.rst:74
msgid "**To install a package**"
msgstr ""

# 1b734453d9d84e5b88b7610989245b8e
#: ../../chef_master/source/resource_yum.rst:78
msgid "**To install a partial minimum version**"
msgstr ""

# a2a8071115014c3eacdd8b5ac433b67b
#: ../../chef_master/source/resource_yum.rst:82
msgid "**To install a specific architecture**"
msgstr ""

# 0c8b0bb232e441fa9471c7a1cf129cbc
#: ../../step_resource/step_resource_yum_package_install_specific_architecture.rst:11
msgid "or:"
msgstr ""

# 85b2fb3a53a34c8d941f4ac216776089
#: ../../chef_master/source/resource_yum.rst:86
msgid "**To install a specific version-release**"
msgstr ""

# b93f66972e334e0a86037d7b8ba95adb
#: ../../chef_master/source/resource_yum.rst:90
msgid "**To install a specific version (even when older than the current)**"
msgstr ""

# d355a54a738e408a9304b89511402a98
#: ../../chef_master/source/resource_yum.rst:94
msgid "**Handle cookbook_file and yum_package resources in the same recipe**"
msgstr ""

# 1841e6e36b9f40d4b1efd0a4a35ee1d8
#: ../../step_resource/step_resource_yum_package_handle_cookbook_file_and_yum_package.rst:5
msgid "When a |resource cookbook_file| resource and a |resource yum_package| resource are both called from within the same recipe, dump the cache and use the new repository immediately to ensure that the correct package is installed:"
msgstr ""

