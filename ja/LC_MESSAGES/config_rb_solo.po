# Japanese translations for Chef Docs package.
# Copyright (C) This work is licensed under a Creative Commons Attribution 3.0 Unported License
# This file is distributed under the same license as the Chef Docs package.
# Automatically generated, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: Chef Docs 0.0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-04-15 09:31\n"
"PO-Revision-Date: 2013-04-15 11:04+0900\n"
"Last-Translator: Naotaka Jay Hotta <fukuzo@cybertron.co.jp>\n"
"Language-Team: none\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 1.5.5\n"

# 1acf2bc863eb4b7499dabbed6669fdc6
#: ../../chef_master/source/config_rb_solo.rst:34
msgid "solo.rb"
msgstr ""

# 540d05cec4414065914132f75cb80c69
#: ../../includes_config/includes_config_rb_solo.rst:4
msgid ""
"|config rb solo| This file is the default configuration file and is loaded "
"every time this executable is run. The |chef solo| executable can be run as "
"a daemon. The configuration file is located at: |path chef solo rb|."
msgstr ""

# c448112542174fe78100a8c7994b38ec
#: ../../chef_master/source/config_rb_solo.rst:39
msgid "Settings"
msgstr ""

# ca88afbfc228450d9f4ba5949a32b067
#: ../../includes_config/includes_config_rb_solo_settings.rst:4
msgid "This configuration file has the following settings:"
msgstr ""

# 1deaf2f7f9f64397a8e6686a589d1a84
#: ../../includes_config/includes_config_rb_solo_settings.rst:10
msgid "Setting"
msgstr ""

# e24dcf82b33c46a8ba1b9aad69faa5d8
#: ../../includes_config/includes_config_rb_solo_settings.rst:11
msgid "Description"
msgstr ""

# d97106a4ec7a4a2599d1b5bffbf49721
#: ../../includes_config/includes_config_rb_solo_settings.rst:12
msgid "``authorized_openid_identifiers``"
msgstr "``authorized_openid_identifiers``"

# 042c7805a5dc47949ffbcca5579bf55f
#: ../../includes_config/includes_config_rb_solo_settings.rst:13
msgid "|authorized_openid_identifiers| For example: ::"
msgstr ""

# 6e4ddcc3caed448994c90118436b9caf
#: ../../includes_config/includes_config_rb_solo_settings.rst:18
msgid "or: ::"
msgstr ""

# 4c6193b46e824085854ba2506a77f6dd
#: ../../includes_config/includes_config_rb_solo_settings.rst:22
msgid "``authorized_openid_providers``"
msgstr "``authorized_openid_providers``"

# 9532f43708f34ae6871391cddab98568
#: ../../includes_config/includes_config_rb_solo_settings.rst:23
msgid "|authorized_openid_providers| For example: ::"
msgstr ""

# 3fdff31668424b288cdf4e4edc544fcb
#: ../../includes_config/includes_config_rb_solo_settings.rst:27
msgid "``cache_type``"
msgstr "``cache_type``"

# cb3cf04aa3b549299f9c45c7e94dd613
#: ../../includes_config/includes_config_rb_solo_settings.rst:28
msgid "|cache_type| For example: ::"
msgstr ""

# a553fdeddc87493c9cc2fd3c1f367e1a
#: ../../includes_config/includes_config_rb_solo_settings.rst:32
msgid "``cache_options``"
msgstr "``cache_options``"

# 91667c45352d48e3a42a8f6626b2f200
#: ../../includes_config/includes_config_rb_solo_settings.rst:33
msgid "|cache_options| For example: ::"
msgstr ""

# c0bc76be7db8497f84aed8c7657f558e
#: ../../includes_config/includes_config_rb_solo_settings.rst:40
msgid "``checksum_path``"
msgstr "``checksum_path``"

# 05bea0561ea344d0b657895f099b94e8
#: ../../includes_config/includes_config_rb_solo_settings.rst:41
msgid "|checksum_path| For example: ::"
msgstr ""

# 50131ea789264ba49b2321cdb096e46b
#: ../../includes_config/includes_config_rb_solo_settings.rst:45
msgid "``cookbook_path``"
msgstr "``cookbook_path``"

# e21a224009ae48a28fff8a5d50be3110
#: ../../includes_config/includes_config_rb_solo_settings.rst:46
msgid "|cookbook_path subdirectory| For example: ::"
msgstr ""

# d23f483f479d4b11847360ae7438eef5
#: ../../includes_config/includes_config_rb_solo_settings.rst:53
msgid "``data_bag_path``"
msgstr "``data_bag_path``"

# 9f8f7012a090444babd2e603c442c4a3
#: ../../includes_config/includes_config_rb_solo_settings.rst:54
msgid "|data_bag_path| For example: ::"
msgstr ""

# d4854c93f3e9491c8a03c1f61f7c2008
#: ../../includes_config/includes_config_rb_solo_settings.rst:58
msgid "``file_cache_path``"
msgstr "``file_cache_path``"

# 2eea0c95ed054912b59ae39eb897b36b
#: ../../includes_config/includes_config_rb_solo_settings.rst:59
msgid "|file_cache_path| For example: ::"
msgstr ""

# 1a02390386b44830bb0e300ccd90615d
#: ../../includes_config/includes_config_rb_solo_settings.rst:63
msgid "``file_backup_path``"
msgstr "``file_backup_path``"

# 3b8071db328045588977e9ecccfbd4ec
#: ../../includes_config/includes_config_rb_solo_settings.rst:64
msgid "|file_backup_path| For example: ::"
msgstr ""

# 2176cdd921984158b48d2388d6a389ac
#: ../../includes_config/includes_config_rb_solo_settings.rst:68
msgid "``json_attribs``"
msgstr "``json_attribs``"

# 98ecc08cdde943ba93863221dfdbb88a
#: ../../includes_config/includes_config_rb_solo_settings.rst:69
msgid "|json_attribs| For example: ::"
msgstr ""

# 0eb9a9be9a7e4fe29d523addab3a33a0
#: ../../includes_config/includes_config_rb_solo_settings.rst:73
#, fuzzy
msgid "``lockfile``"
msgstr "``log_level``"

# 0f9088262e024a538efbd51c4b63c64c
#: ../../includes_config/includes_config_rb_solo_settings.rst:74
msgid "|lockfile| For example: ::"
msgstr ""

# 0eb9a9be9a7e4fe29d523addab3a33a0
#: ../../includes_config/includes_config_rb_solo_settings.rst:78
msgid "``log_level``"
msgstr "``log_level``"

# 2b74b62bf7f5463e9749777ee9336439
#: ../../includes_config/includes_config_rb_solo_settings.rst:79
msgid "|log_level| For example: ::"
msgstr ""

# 3ab884d2694747f19443580189f3ccc5
#: ../../includes_config/includes_config_rb_solo_settings.rst:83
msgid "``log_location``"
msgstr "``log_location``"

# e34f028d94314821af0b4a0361577a65
#: ../../includes_config/includes_config_rb_solo_settings.rst:84
msgid "|log_location| For example: ::"
msgstr ""

# a319188ed6bb4cacbd492b43c59d1262
#: ../../includes_config/includes_config_rb_solo_settings.rst:88
msgid "``node_name``"
msgstr "``node_name``"

# df1e0b5caf2c4676a250dd43cc75fada
#: ../../includes_config/includes_config_rb_solo_settings.rst:89
msgid "|node_name| For example: ::"
msgstr ""

# be6bdac74e674461b45b7bc9ee3dc4cc
#: ../../includes_config/includes_config_rb_solo_settings.rst:93
msgid "``node_path``"
msgstr "``node_path``"

# 574f0b3f58a54a8c959c8bfd58860f93
#: ../../includes_config/includes_config_rb_solo_settings.rst:94
msgid "|node_path| For example: ::"
msgstr ""

# 3fe8a6e6653c464c8e0394baf7f90c42
#: ../../includes_config/includes_config_rb_solo_settings.rst:98
msgid "``openid_cstore_couchdb``"
msgstr "``openid_cstore_couchdb``"

# 84af4a266e124e819a9a7e624ffc749d
#: ../../includes_config/includes_config_rb_solo_settings.rst:99
msgid "|openid_cstore_couchdb| For example: ::"
msgstr ""

# cb36ce97fb0842de9d09ab3fcd6a3a01
#: ../../includes_config/includes_config_rb_solo_settings.rst:103
msgid "``openid_cstore_path``"
msgstr "``openid_cstore_path``"

# 6ec86c27c8ea410085440229e3116c8a
#: ../../includes_config/includes_config_rb_solo_settings.rst:104
msgid "|openid_cstore_path| For example: ::"
msgstr ""

# fb0118f7fb7c4211802497c0be635a0f
#: ../../includes_config/includes_config_rb_solo_settings.rst:108
msgid "``recipe_url``"
msgstr "``recipe_url``"

# 03949e745f5f48809d0d2429d7a01079
#: ../../includes_config/includes_config_rb_solo_settings.rst:109
msgid "|recipe_url| For example: ::"
msgstr ""

# 6938f7dda624458e95e0357f61b89e18
#: ../../includes_config/includes_config_rb_solo_settings.rst:113
msgid "``rest_timeout``"
msgstr "``rest_timeout``"

# 216516a6e40a4cae9c4df62129d9bb6e
#: ../../includes_config/includes_config_rb_solo_settings.rst:114
msgid "|rest_timeout| For example: ::"
msgstr ""

# 3b46fd49671d4150834f4e4c358e39b3
#: ../../includes_config/includes_config_rb_solo_settings.rst:118
msgid "``role_path``"
msgstr "``role_path``"

# 1d3abb7403b146cbb1961967162d3d2e
#: ../../includes_config/includes_config_rb_solo_settings.rst:119
msgid "|role_path| For example: ::"
msgstr ""

# f9b0e4b7cbf9449ebf7c14be0f7996d8
#: ../../includes_config/includes_config_rb_solo_settings.rst:123
msgid "``sandbox_path``"
msgstr "``sandbox_path``"

# e35d880f5fb547938c4f3c819c99795c
#: ../../includes_config/includes_config_rb_solo_settings.rst:124
msgid "|sandbox_path| For example: ::"
msgstr ""

# 91ebd74d6bdf496dbcdc345302a130ba
#: ../../includes_config/includes_config_rb_solo_settings.rst:128
msgid "``solo``"
msgstr "``solo``"

# c4e5708f65f14e18b163044f5d473de9
#: ../../includes_config/includes_config_rb_solo_settings.rst:129
msgid "|solo mode| For example: ::"
msgstr ""

# e23de66131484b35b24722cd5180f8a3
#: ../../includes_config/includes_config_rb_solo_settings.rst:133
msgid "``umask``"
msgstr "``umask``"

# 86b9a31767794b23bb8dd3751e020436
#: ../../includes_config/includes_config_rb_solo_settings.rst:134
msgid "|umask| For example: ::"
msgstr ""

# 15f10557775b40c2b27ff6b925a5a6e9
#: ../../includes_config/includes_config_rb_solo_settings.rst:138
msgid "``verbose_logging``"
msgstr "``verbose_logging``"

# cb38cd7c497448b28032c04fbabfb805
#: ../../includes_config/includes_config_rb_solo_settings.rst:139
msgid ""
"|verbose_logging| For example, when ``verbose_logging`` is set to ``true`` "
"or ``nil``: ::"
msgstr ""

# 622e58f8fe3f46bf99951db8b11ce2ad
#: ../../includes_config/includes_config_rb_solo_settings.rst:163
msgid "When ``verbose_logging`` is set to ``false`` (for the same output): ::"
msgstr ""

# f14304c5141948ce977dc90bfe2541f3
#: ../../includes_config/includes_config_rb_solo_settings.rst:179
msgid ""
"Where in the examples above, ``[date]`` represents the date and time the "
"long entry was created. For example: ``[Mon, 21 Nov 2011 09:37:39 -0800]``."
msgstr ""
