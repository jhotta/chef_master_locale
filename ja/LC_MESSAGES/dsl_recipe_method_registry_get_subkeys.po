# Japanese translations for Chef Docs package.
# Copyright (C) This work is licensed under a Creative Commons Attribution 3.0 Unported License
# This file is distributed under the same license as the Chef Docs package.
# Automatically generated, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: Chef Docs 0.0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-04-15 09:31\n"
"PO-Revision-Date: 2013-04-15 11:04+0900\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 1.5.5\n"

# 13a986bfcfa1463f923b28c7d42ac871
#: ../../chef_master/source/dsl_recipe_method_registry_get_subkeys.rst:34
msgid "registry_get_subkeys Method"
msgstr ""

# f9089e728d714c95b512c965fb40c6d5
#: ../../includes_dsl_recipe/includes_dsl_recipe_method_registry_get_subkeys.rst:4
msgid ""
"The ``registry_get_subkeys`` method can be used in a recipe to get a list of "
"sub-keys that are present for a |windows| registry setting."
msgstr ""

# 23deeb5d65664ad9bed2b03fcea45609
#: ../../includes_dsl_recipe/includes_dsl_recipe_method_registry_get_subkeys.rst:6
msgid "|note registry_key not_if only_if|"
msgstr ""

# 000052dde8c345d1b01e1b826a2b7a56
#: ../../includes_dsl_recipe/includes_dsl_recipe_method_registry_get_subkeys.rst:8
msgid "The syntax for the ``registry_get_subkeys`` method is as follows:"
msgstr ""

# bab0075927cc4f59aa3fe221f8b4bcee
#: ../../includes_dsl_recipe/includes_dsl_recipe_method_registry_get_subkeys.rst:14
msgid "where"
msgstr ""

# 1c0637c763a84e0ba61952640410436c
#: ../../includes_dsl_recipe/includes_dsl_recipe_method_registry_get_subkeys.rst:16
msgid ""
"``KEY_PATH`` is the path to the registry key. |key_name resource "
"registry_key hives|"
msgstr ""

# 83bf46bcc6114ae79d927bed5427eb17
#: ../../includes_dsl_recipe/includes_dsl_recipe_method_registry_get_subkeys.rst:17
msgid ""
"``ARCHITECTURE`` is one of the following values: ``:x86_64``, ``:i386``, or "
"``:machine``. |architecture resource registry_key machine|"
msgstr ""

# 4e558fc28ba5436582047fc46b2c3fb1
#: ../../includes_dsl_recipe/includes_dsl_recipe_method_registry_get_subkeys.rst:19
msgid "The results of ``registry_get_subkeys`` is an array of sub-keys."
msgstr ""

# 09a46e897f044f3a84244d839cdcfa54
#: ../../includes_dsl_recipe/includes_dsl_recipe_method_registry_get_subkeys.rst:21
msgid "|note registry_key architecture|"
msgstr ""
