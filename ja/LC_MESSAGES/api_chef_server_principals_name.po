# Japanese translations for Chef Docs package.
# Copyright (C) This work is licensed under a Creative Commons Attribution 3.0 Unported License
# This file is distributed under the same license as the Chef Docs package.
# Automatically generated, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: Chef Docs 0.0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-04-15 09:31\n"
"PO-Revision-Date: 2013-04-15 11:04+0900\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 1.5.5\n"

# 9b2edee124fe44e19e247eec47167782
#: ../../chef_master/source/api_chef_server_principals_name.rst:34
msgid "/principals/NAME"
msgstr ""

# faf695fbdaf146ab88474e215acbf45e
#: ../../includes_api_chef_server/includes_api_chef_server_endpoint_principals_name.rst:4
msgid "The /principals/NAME endpoint has the following methods: GET."
msgstr ""

# 89733868538b4562a1ec629c14b46c51
#: ../../chef_master/source/api_chef_server_principals_name.rst:39
msgid "GET"
msgstr ""

# bcc2cba3f2cb4fbeb9ac75890fcd09f9
#: ../../includes_api_chef_server/includes_api_chef_server_endpoint_principals_name_get.rst:4
msgid ""
"The GET method is used to get a list of public keys for clients and users in "
"order to ensure that enough information is present for authorized requests."
msgstr ""

# a60fea9c84314a63b317adc6b3123e4e
#: ../../includes_api_chef_server/includes_api_chef_server_endpoint_principals_name_get.rst:6
msgid "This method has no parameters."
msgstr ""

# 91fe99de2dff4280b87b321a664d0a3f
#: ../../includes_api_chef_server/includes_api_chef_server_endpoint_principals_name_get.rst:8
msgid "**Request**"
msgstr ""

# ce159dbb851d441d8c4a6762ec3dc97c
#: ../../includes_api_chef_server/includes_api_chef_server_endpoint_principals_name_get.rst:14
msgid "**Response**"
msgstr ""

# 262aaf0d32db46869b14695d9f402f79
#: ../../includes_api_chef_server/includes_api_chef_server_endpoint_principals_name_get.rst:16
msgid "The response will return something like the following for a user:"
msgstr ""

# 6b696924e2424d5d810bd72edf81b2db
#: ../../includes_api_chef_server/includes_api_chef_server_endpoint_principals_name_get.rst:31
msgid "or something like the following for a client:"
msgstr ""

# 226edf788381460b859a637db6c6a011
#: ../../includes_api_chef_server/includes_api_chef_server_endpoint_principals_name_get.rst:46
msgid "**Response Codes**"
msgstr ""

# 0a205d1e50cc47df8389dba247ee0141
#: ../../includes_api_chef_server/includes_api_chef_server_endpoint_principals_name_get.rst:52
msgid "Response Code"
msgstr ""

# 40e7e2ba1f1b40018e39430fa4b8cffe
#: ../../includes_api_chef_server/includes_api_chef_server_endpoint_principals_name_get.rst:53
msgid "Description"
msgstr ""

# 2e03fab5a39f473d9cdcaf41248b0d1e
#: ../../includes_api_chef_server/includes_api_chef_server_endpoint_principals_name_get.rst:54
msgid "``200``"
msgstr ""

# 2afea77a76b740f3aae0a93dd121945f
#: ../../includes_api_chef_server/includes_api_chef_server_endpoint_principals_name_get.rst:55
msgid "|response code 200 ok|"
msgstr ""

# 07ce1ad31bef4b4388ff7fc66f3fb01a
#: ../../includes_api_chef_server/includes_api_chef_server_endpoint_principals_name_get.rst:56
msgid "``404``"
msgstr ""

# 1ea7bf2b0f2d43c29e8fbdf30e249c45
#: ../../includes_api_chef_server/includes_api_chef_server_endpoint_principals_name_get.rst:57
msgid "|response code 404 not found|"
msgstr ""
